#-------------------------------------------------
#
# Project created by QtCreator 2011-11-07T20:17:28
#
#-------------------------------------------------

! include( ../../common.pri ) {
    error( Couldn't find the common.pri file! )
}


QT       += core

QT       -= gui

TARGET = TestInt32StringProtocol
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += \
    testint32stringprotocol.cpp

HEADERS += \
    ../../rpc/int32stringprotocol.h \
    ../../rpc/server.h \
    ../../rpc/common.h





