#define BOOST_TEST_MODULE "C++ Unit Tests for Int32StringProtocol"

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include <string>
#include <vector>

#include "rpc/int32stringprotocol.h"


#define _HAS_ITERATOR_DEBUGGING 1

using namespace protorpc;
using namespace std;
using namespace boost::asio::ip;

namespace asio = boost::asio;

struct TestProtocol : public Int32StringProtocol {

    int errors;
    int lost;

    virtual void string_received(const string &msg) {
        cerr << _name << ": string_received" << endl;
        messages.push_back(msg);
    }

    virtual void error_received(boost::system::error_code error) {
        cerr << _name << ": error_received" << endl;
        errors;
    }

    virtual void connection_lost() {
        cerr << _name << ": connection_lost" << endl;
        lost++;
    }

    TestProtocol(string name, asio::io_service& io_service) : Int32StringProtocol(io_service), _name(name) {
        errors = lost = 0;
    }

    vector<string> messages;
    string _name;

};

struct TestFixture {
    asio::io_service _io_service;
    boost::shared_ptr<asio::ip::tcp::acceptor> _acceptor;
    boost::shared_ptr<TestProtocol> _server;
    boost::shared_ptr<TestProtocol> _client;
    asio::deadline_timer _timer;
    bool error_connecting;
    int num_write_operation;
    bool write_callback;

    TestFixture() : _io_service(),
                    _timer(_io_service) {
        cerr << "Initializing fixture" << endl;

        set_acceptor(22222);

        _server = boost::shared_ptr<TestProtocol>(new TestProtocol("Server", _io_service));
        _client = boost::shared_ptr<TestProtocol>(new TestProtocol("Client", _io_service));

        error_connecting = false;
        write_callback = false;

        /*
        start_accept();
        _client->resolve_connect_and_start("localhost", "22222");
        */
    }

    void set_acceptor(short port) {
        _acceptor = boost::shared_ptr<asio::ip::tcp::acceptor>(
                    new asio::ip::tcp::acceptor(_io_service, tcp::endpoint(tcp::v4(), port)));
    }

    void start_accept_testSendMessages() {
        cerr << "Starting accept" << endl;
        _acceptor->async_accept(_server->socket(), boost::bind(&TestFixture::handle_accept_testSendMessages, this,
                              boost::asio::placeholders::error));
    }

    void handle_accept_testSendMessages(const boost::system::error_code &error) {
        cerr << "handle accept" << endl;
        if(!error) {
            _server->start();
        } else {
            cerr << "Error when accepting" << endl;
            BOOST_FAIL("Error when accepting");
        }


        // Server connected -> we send messages
        _server->send_string("Message from server to client");
        _client->send_string("Message from client to server 1");
        _client->send_string("Message from client to server 2");
    }

    void start_accept_testWriteCallback() {
        cerr << "Starting accept" << endl;
        _acceptor->async_accept(_server->socket(), boost::bind(&TestFixture::handle_accept_testWriteCallback, this,
                              boost::asio::placeholders::error));
    }

    void handle_accept_testWriteCallback(const boost::system::error_code &error) {
        cerr << "handle accept" << endl;
        if(!error) {
            _server->start();
        } else {
            cerr << "Error when accepting" << endl;
            BOOST_FAIL("Error when accepting");
        }


        // Server connected -> we send messages
        num_write_operation = _server->send_string("Message from server to client",
                                                   boost::bind(&TestFixture::handle_write_callback, this,
                                                               _1,
                                                               _2));
    }

    void handle_write_callback(unsigned int _write_op, boost::system::error_code error) {

        if(_write_op == num_write_operation)
            write_callback = true;
    }

    void handle_connect(boost::system::error_code error) {
        if(error)
            error_connecting = true;
    }

    void handle_close(const boost::system::error_code &error) {
        if(error) {
            BOOST_FAIL("Error in timer");
            return;
        }

        _server->nice_close();
    }
};

BOOST_FIXTURE_TEST_SUITE(TestInt32StringProtocol, TestFixture);

//____________________________________________________________________________//

BOOST_AUTO_TEST_CASE( testCannotConnect )
{
    // Close the server in 1000 milliseconds
    _timer.expires_from_now(boost::posix_time::milliseconds(1000));
    _timer.async_wait(boost::bind(&TestFixture::handle_close,
                                      this,
                                      asio::placeholders::error));

    // no server
    _client->resolve_connect_and_start("localhost", "22221", boost::bind(&TestFixture::handle_connect, this,
                                                                         asio::placeholders::error));
    _io_service.run();

    BOOST_REQUIRE( error_connecting );
}

//____________________________________________________________________________//

BOOST_AUTO_TEST_CASE( testSendMessages )
{
    // Close the server in 1000 milliseconds
    _timer.expires_from_now(boost::posix_time::milliseconds(1000));
    _timer.async_wait(boost::bind(&TestFixture::handle_close,
                                      this,
                                      asio::placeholders::error));

    start_accept_testSendMessages();
    _client->resolve_connect_and_start("localhost", "22222");

    _io_service.run();

    BOOST_REQUIRE( !error_connecting );
    BOOST_REQUIRE( _server->messages.size() == 2 );
    BOOST_REQUIRE( _server->messages[0] == "Message from client to server 1" );
    BOOST_REQUIRE( _server->messages[1] == "Message from client to server 2" );
    BOOST_REQUIRE( _server->errors == 0);
    BOOST_REQUIRE( _server->lost == 1);

    BOOST_REQUIRE( _client->messages.size() == 1 );
    BOOST_REQUIRE( _client->messages[0] == "Message from server to client" );
    BOOST_REQUIRE( _client->errors == 0);
    BOOST_REQUIRE( _client->lost == 1);
}

//____________________________________________________________________________//

BOOST_AUTO_TEST_CASE( testWriteCallback )
{
    // Close the server in 1000 milliseconds
    _timer.expires_from_now(boost::posix_time::milliseconds(1000));
    _timer.async_wait(boost::bind(&TestFixture::handle_close,
                                      this,
                                      asio::placeholders::error));

    start_accept_testWriteCallback();
    _client->resolve_connect_and_start("localhost", "22222");

    _io_service.run();

    BOOST_REQUIRE( !error_connecting );
    BOOST_REQUIRE( write_callback == true );
}


BOOST_AUTO_TEST_SUITE_END();

