#-------------------------------------------------
#
# Project created by QtCreator 2011-11-08T21:01:27
#
#-------------------------------------------------
! include( ../../common.pri ) {
    error( Couldn't find the common.pri file! )
}


QT       += core

QT       -= gui

TARGET = TestingProtocolBuffersChannel
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += \
    testingprotocolbufferschannel.cpp \
    ../../rpc/protobufrpc.pb.cc \
    ../test.pb.cc

HEADERS += \
    ../../rpc/int32stringprotocol.h \
    ../../rpc/server.h \
    ../../rpc//common.h \
    ../../rpc/protocolbufferschannel.h \
    ../../rpc/protobufrpc.pb.h \
    ../test.pb.h

OTHER_FILES += \
    ../test.proto


