#define BOOST_TEST_MODULE "C++ Unit Tests for ProtocolBuffersChannel"

#include <boost/shared_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include <string>
#include <vector>

#include "rpc/protocolbufferschannel.h"
#include "test.pb.h"

using namespace protorpc;
using namespace std;
using namespace boost::asio::ip;

namespace asio = boost::asio;

class TestServiceImpl : public test::Test {
public:
    virtual void Ping(::google::protobuf::RpcController* controller,
                       const ::test::PingRequest* request,
                       ::test::PingResponse* response,
                       ::google::protobuf::Closure* done) {

        done->Run();
    }

    virtual void Echo(::google::protobuf::RpcController* controller,
                       const ::test::EchoRequest* request,
                       ::test::EchoResponse* response,
                       ::google::protobuf::Closure* done) {
        response->set_text(request->text());

        done->Run();
    }
};

class MathServiceImpl : public test::Math {
public:
    virtual void Add(::google::protobuf::RpcController* controller,
                       const ::test::MathBinaryOperationRequest* request,
                       ::test::MathResponse* response,
                       ::google::protobuf::Closure* done) {
        response->set_result(request->first() + request->second());
        done->Run();
    }

    virtual void Multiply(::google::protobuf::RpcController* controller,
                       const ::test::MathBinaryOperationRequest* request,
                       ::test::MathResponse* response,
                       ::google::protobuf::Closure* done) {
        response->set_result(request->first() * request->second());
        done->Run();
    }
};

struct TestFixture {
    asio::io_service _io_service;
    boost::shared_ptr<asio::ip::tcp::acceptor> _acceptor;
    boost::shared_ptr<ProtocolBuffersChannel> _server;
    boost::shared_ptr<ProtocolBuffersChannel> _client;
    asio::deadline_timer _timer;
    bool error_connecting;
    string echo_string;
    float addition;
    float multiplication;
    string echo_string_from_client;

    TestFixture() : _io_service(), _timer(_io_service)
    {
        _server = boost::shared_ptr<ProtocolBuffersChannel>(new ProtocolBuffersChannel(_io_service));
        _server->add_service(boost::shared_ptr<pb::Service>(new TestServiceImpl));
        _server->add_service(boost::shared_ptr<pb::Service>(new MathServiceImpl));
        _client = boost::shared_ptr<ProtocolBuffersChannel>(new ProtocolBuffersChannel(_io_service));
        _client->add_service(boost::shared_ptr<pb::Service>(new MathServiceImpl));

        error_connecting = false;
        addition = 0.0;
        multiplication = 0.0;
    }

    void set_acceptor(short port) {
        _acceptor = boost::shared_ptr<asio::ip::tcp::acceptor>(
                    new asio::ip::tcp::acceptor(_io_service, tcp::endpoint(tcp::v4(), port)));
    }

    void start_accept_testServerEcho() {
        cerr << "Starting accept" << endl;
        _acceptor->async_accept(_server->socket(), boost::bind(&TestFixture::handle_accept_testServerEcho, this,
                              boost::asio::placeholders::error));
    }

    void handle_accept_testServerEcho(const boost::system::error_code &error) {
        cerr << "handle accept" << endl;
        if(!error) {
            _server->start();
        } else {
            cerr << "Error when accepting" << endl;
            BOOST_FAIL("Error when accepting");
        }

        test::Test_Stub serviceStub(_client.get());
        pb::RpcController *controller = new ProtocolBuffersController;
        test::EchoRequest request;
        request.set_text("My name is Luis");
        test::EchoResponse *response = new test::EchoResponse;
        serviceStub.Echo(controller, &request, response, pb::NewCallback(this, &TestFixture::handle_echo, controller, response));

        pb::RpcController *c2 = new ProtocolBuffersController;
        test::Math_Stub mathServiceStub(_client.get());
        test::MathBinaryOperationRequest opRequest;
        opRequest.set_first(25);
        opRequest.set_second(25);
        test::MathResponse *mathResponse = new test::MathResponse;
        mathServiceStub.Add(c2, &opRequest, mathResponse, pb::NewCallback(this, &TestFixture::handle_addition, c2, mathResponse));
    }

    void start_accept_testClientDoesMath() {
        cerr << "Starting accept" << endl;
        _acceptor->async_accept(_server->socket(), boost::bind(&TestFixture::handle_accept_testClientDoesMath, this,
                              boost::asio::placeholders::error));
    }

    void handle_accept_testClientDoesMath(const boost::system::error_code &error) {
        cerr << "handle accept" << endl;
        if(!error) {
            _server->start();
        } else {
            cerr << "Error when accepting" << endl;
            BOOST_FAIL("Error when accepting");
        }

        test::Test_Stub serviceStub(_client.get());

        pb::RpcController *controller = new ProtocolBuffersController;
        test::EchoRequest request;
        request.set_text("My name is Luis");
        test::EchoResponse *response = new test::EchoResponse;
        serviceStub.Echo(controller, &request, response, pb::NewCallback(this, &TestFixture::handle_echo, controller, response));

        pb::RpcController *c2 = new ProtocolBuffersController;
        test::Math_Stub mathStub(_server.get());    // Notice how the server calls the operation in the client
        test::MathBinaryOperationRequest opRequest;
        opRequest.set_first(25);
        opRequest.set_second(25);
        test::MathResponse *mathResponse = new test::MathResponse;
        mathStub.Multiply(c2, &opRequest, mathResponse, pb::NewCallback(this, &TestFixture::handle_multiplication, c2, mathResponse));
    }

    void start_accept_testClientCannotDoEcho() {
        cerr << "Starting accept" << endl;
        _acceptor->async_accept(_server->socket(), boost::bind(&TestFixture::handle_accept_testClientDoesMath, this,
                              boost::asio::placeholders::error));
    }

    void handle_accept_testClientCannotDoEcho(const boost::system::error_code &error) {
        cerr << "handle accept" << endl;
        if(!error) {
            _server->start();
        } else {
            cerr << "Error when accepting" << endl;
            BOOST_FAIL("Error when accepting");
        }

        test::Test_Stub serviceStub(_client.get());

        pb::RpcController *controller = new ProtocolBuffersController;
        test::EchoRequest request;
        request.set_text("My name is Luis");
        test::EchoResponse *response = new test::EchoResponse;
        serviceStub.Echo(controller, &request, response, pb::NewCallback(this, &TestFixture::handle_echo, controller, response));

        pb::RpcController *c2 = new ProtocolBuffersController;
        test::Test_Stub serverAskingEcho(_server.get());
        test::EchoResponse *responseFromClient = new test::EchoResponse;
        serverAskingEcho.Echo(c2, &request, responseFromClient, pb::NewCallback(this, &TestFixture::handle_echo_from_client, c2,
                                                                                         responseFromClient));
    }


    void handle_echo(pb::RpcController *controller, test::EchoResponse *response) {
        if(!controller->Failed())
            echo_string = response->text();

        delete response;
        delete controller;
    }

    void handle_addition(pb::RpcController *controller, test::MathResponse *response) {
        if(!controller->Failed())
            addition = response->result();
        delete response;
        delete controller;
    }

    void handle_multiplication(pb::RpcController *controller, test::MathResponse *response) {
        if(!controller->Failed())
            multiplication = response->result();
        delete response;
        delete controller;
    }

    void handle_echo_from_client(pb::RpcController *controller, test::EchoResponse *response) {

        if(!controller->Failed())
            echo_string_from_client = response->text();

        delete response;
        delete controller;
    }

    void handle_connect(boost::system::error_code error) {
        if(error)
            error_connecting = true;
    }

    void handle_close(const boost::system::error_code &error) {
        if(error) {
            BOOST_FAIL("Error in timer");
            return;
        }

        _server->nice_close();
    }
};

BOOST_FIXTURE_TEST_SUITE(TestProtocolBuffersChannel, TestFixture);

BOOST_AUTO_TEST_CASE( testServerEcho )
{
    // Close the server in 1000 milliseconds
    _timer.expires_from_now(boost::posix_time::milliseconds(1000));
    _timer.async_wait(boost::bind(&TestFixture::handle_close,
                                      this,
                                      asio::placeholders::error));


    set_acceptor(22222);
    start_accept_testServerEcho();
    _client->resolve_connect_and_start("localhost", "22222", boost::bind(&TestFixture::handle_connect, this,
                                                                         asio::placeholders::error));

    _io_service.run();

    BOOST_REQUIRE( error_connecting == false );
    BOOST_REQUIRE( echo_string == "My name is Luis" );
    BOOST_REQUIRE( addition == 50 );
}

BOOST_AUTO_TEST_CASE( testClientDoesMath )
{
    // Close the server in 1000 milliseconds
    _timer.expires_from_now(boost::posix_time::milliseconds(1000));
    _timer.async_wait(boost::bind(&TestFixture::handle_close,
                                      this,
                                      asio::placeholders::error));


    set_acceptor(22222);
    start_accept_testClientDoesMath();
    _client->resolve_connect_and_start("localhost", "22222", boost::bind(&TestFixture::handle_connect, this,
                                                                         asio::placeholders::error));

    _io_service.run();

    BOOST_REQUIRE( error_connecting == false );
    BOOST_REQUIRE( echo_string == "My name is Luis" );
    BOOST_REQUIRE( multiplication == 625 );
}

BOOST_AUTO_TEST_CASE( testClientCannotDoEcho )
{
    // Close the server in 1000 milliseconds
    _timer.expires_from_now(boost::posix_time::milliseconds(1000));
    _timer.async_wait(boost::bind(&TestFixture::handle_close,
                                      this,
                                      asio::placeholders::error));


    set_acceptor(22222);
    start_accept_testClientCannotDoEcho();
    _client->resolve_connect_and_start("localhost", "22222", boost::bind(&TestFixture::handle_connect, this,
                                                                         asio::placeholders::error));

    _io_service.run();

    BOOST_REQUIRE( error_connecting == false );
    BOOST_REQUIRE( echo_string == "My name is Luis" );
    BOOST_REQUIRE( echo_string_from_client == "");

}


BOOST_AUTO_TEST_SUITE_END();
