
INCLUDEPATH += . .. ../..
WARNINGS += -Werror
UI_DIR = uics
MOC_DIR = mocs
OBJECTS_DIR = objs

unix {
INCLUDEPATH += /opt/local/include

LIBS += -L/opt/local/lib
LIBS += -lboost_test_exec_monitor -lboost_system -lboost_thread-mt -lprotobuf
}

win32 {
INCLUDEPATH += C:\\utils\\protobuf\\src
INCLUDEPATH += C:\\utils\\boost\\boost_1_47

LIBS += -LC:\\utils\\boost\\boost_1_47\\lib
#LIBS += -llibboost_system-vc90-mt-1_47 -llibboost_test_exec_monitor-vc90-sgd-1_47
LIBS += -llibboost_system-vc90-mt-1_47
LIBS += -L"C:\\Documents and Settings\\e019513\\My Documents\\protobuf-2.4.1\\vsprojects\\Debug" -llibprotobuf
}
