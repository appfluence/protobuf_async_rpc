
#include <boost/shared_ptr.hpp>

#include "rpc/int32stringprotocol.h"

namespace asio = boost::asio;

using namespace std;
using namespace protorpc;

class Server {

public:
    Server(asio::io_service &io_service, unsigned short port = 8080)
        : _acceptor(io_service, tcp::endpoint(tcp::v4(), port)) {
    }

    void start_accept() {
        channel = boost::shared_ptr<Int32StringProtocol>(
                    new Int32StringProtocol(_acceptor.get_io_service()));

        _acceptor.async_accept(channel->socket(), boost::bind(&Server::handle_accept, this,
                                                              channel, boost::asio::placeholders::error));
    }

private:
    asio::ip::tcp::acceptor _acceptor;
    boost::shared_ptr<Int32StringProtocol> channel;

    void handle_accept(boost::shared_ptr<Int32StringProtocol> channel,
                       const boost::system::error_code &error) {
        if(!error) {
            channel->start();
        } else {
            std::cerr << "Error when accepting connection" << std::endl;
        }
    }
};


int main(int argc, char *argv[])
{
    cerr << "Listening in 8080" << endl;
    asio::io_service io_service;

    Server server(io_service, 8080);
    server.start_accept();

    io_service.run();
}
