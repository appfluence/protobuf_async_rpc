#-------------------------------------------------
#
# Project created by QtCreator 2011-11-11T17:06:21
#
#-------------------------------------------------

! include( ../../common.pri ) {
    error( Couldn't find the common.pri file! )
}


QT       += core

QT       -= gui

TARGET = EchoServerInt32StringProtocol
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

