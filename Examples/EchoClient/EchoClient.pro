#-------------------------------------------------
#
# Project created by QtCreator 2011-11-11T10:34:46
#
#-------------------------------------------------
! include( ../../common.pri ) {
    error( Couldn't find the common.pri file! )
}


QT       += core

QT       -= gui

TARGET = EchoClient
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    ../../rpc/protobufrpc.pb.cc \
    ../test.pb.cc

HEADERS += \
    ../test.pb.h

