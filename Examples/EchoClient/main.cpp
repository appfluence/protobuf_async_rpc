
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <iostream>

#include "rpc/protocolbufferschannel.h"
#include "rpc/server.h"
#include "test.pb.h"

#define _HAS_ITERATOR_DEBUGGING 0

using namespace std;
using namespace protorpc;

class Client : public test::Test_Stub {
public:
    Client(boost::shared_ptr<ProtocolBuffersChannel> channel) :
        test::Test_Stub(channel.get()),
        _channel(channel) {
    }

    void start(string host, string port) {
         _channel->resolve_connect_and_start(host, port,
                                            boost::bind(&Client::handle_connect, this,
                                                         asio::placeholders::error));
    }

    virtual void string_received(const string &msg) {
        cerr << msg << endl;
    }

private:

    void handle_connect(const boost::system::error_code &error) {
        if(error) {
            cerr << "Error " << endl;
            return;
        }

        call_echo_service();
    }

    void call_echo_service() {

        pb::RpcController *controller = new ProtocolBuffersController;
        test::EchoRequest request;
        request.set_text("My name is Luis");
        test::EchoResponse *response = new test::EchoResponse;
        this->Echo(controller, &request, response, pb::NewCallback(this, &Client::handle_echo, controller, response));
    }

    void handle_echo(pb::RpcController *controller, test::EchoResponse *response) {

        if(controller->Failed()) {
            cerr << "Echo failed. Server returned: " << controller->ErrorText() << endl;
        } else {
            cerr << "Echo succeded. Server returned: " << response->text() << endl;
        }
        delete response;
        delete controller;

        call_echo_service();
    }

    boost::shared_ptr<ProtocolBuffersChannel> _channel;
};


int main(int argc, char *argv[])
{
    cerr << "connecting to 8080" << endl;
    asio::io_service io_service;

    boost::shared_ptr<ProtocolBuffersChannel> channel(new ProtocolBuffersChannel(io_service));
    Client client(channel);
    client.start("localhost", "8080");

    io_service.run();
}
