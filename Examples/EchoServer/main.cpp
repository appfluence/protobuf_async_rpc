
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <iostream>

#include "rpc/protocolbufferschannel.h"
#include "rpc/server.h"
#include "test.pb.h"

#define _HAS_ITERATOR_DEBUGGING 0

namespace asio = boost::asio;
namespace pb = google::protobuf;
using namespace std;
using namespace protorpc;

class TestServiceImpl : public test::Test {
public:
    virtual void Ping(::google::protobuf::RpcController* controller,
                       const ::test::PingRequest* request,
                       ::test::PingResponse* response,
                       ::google::protobuf::Closure* done) {

        done->Run();
    }

    virtual void Echo(::google::protobuf::RpcController* controller,
                       const ::test::EchoRequest* request,
                       ::test::EchoResponse* response,
                       ::google::protobuf::Closure* done) {
        response->set_text(request->text());

        done->Run();
    }
};


int main(int argc, char *argv[])
{
    cerr << "Listening in 8080" << endl;
    asio::io_service io_service;
    boost::shared_ptr<pb::Service> service(new TestServiceImpl);

    Server<ProtocolBuffersChannel> server(io_service, 8080);
    server.add_service(service);
    server.start_accept();

    io_service.run();
}
