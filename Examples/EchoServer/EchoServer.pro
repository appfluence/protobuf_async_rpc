#-------------------------------------------------
#
# Project created by QtCreator 2011-11-11T10:36:32
#
#-------------------------------------------------
! include( ../../common.pri ) {
    error( Couldn't find the common.pri file! )
}


QT       += core

QT       -= gui

TARGET = EchoServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
        ../../rpc/protobufrpc.pb.cc \
        ../test.pb.cc

INCLUDEPATH += C:\\utils\\boost\\boost_1_47

HEADERS += \
    ../test.pb.h




