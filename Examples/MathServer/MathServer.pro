#-------------------------------------------------
#
# Project created by QtCreator 2011-11-12T14:23:07
#
#-------------------------------------------------

! include( ../../common.pri ) {
    error( Couldn't find the common.pri file! )
}


QT       += core

QT       -= gui

TARGET = MathServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
        ../../rpc/protobufrpc.pb.cc \
        ../math.pb.cc

OTHER_FILES += \
    ../math.proto

