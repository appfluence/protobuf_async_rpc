#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <iostream>
#include <string>

#include "rpc/protocolbufferschannel.h"
#include "rpc/server.h"
#include "math.pb.h"

namespace asio = boost::asio;
namespace pb = google::protobuf;
using namespace std;
using namespace protorpc;

class MathServiceImpl : public math::Math {         // WE IMPLEMENT THE SERVICE PROVIDED IN math.pb.h
public:
    virtual void Add(::google::protobuf::RpcController* controller,
                       const ::math::MathBinaryOperationRequest* request,
                       ::math::MathResponse* response,
                       ::google::protobuf::Closure* done) {
        response->set_result(request->first() + request->second());
        done->Run();
    }
    virtual void Substract(::google::protobuf::RpcController* controller,
                       const ::math::MathBinaryOperationRequest* request,
                       ::math::MathResponse* response,
                       ::google::protobuf::Closure* done) {
        response->set_result(request->first() - request->second());
        done->Run();
    }
    virtual void Multiply(::google::protobuf::RpcController* controller,
                       const ::math::MathBinaryOperationRequest* request,
                       ::math::MathResponse* response,
                       ::google::protobuf::Closure* done) {
        response->set_result(request->first() * request->second());
        done->Run();
    }
    virtual void Divide(::google::protobuf::RpcController* controller,
                       const ::math::MathBinaryOperationRequest* request,
                       ::math::MathResponse* response,
                       ::google::protobuf::Closure* done) {
        if(request->second() == 0) {
            controller->SetFailed("Cannot divide by 0");
        } else {
            response->set_result(request->first() / request->second());
        }

        done->Run();
    }
};

int main(int argc, char *argv[])
{
    cerr << "Listening in 8080" << endl;
    asio::io_service io_service;        // This is the async loop
    boost::shared_ptr<pb::Service> service(new MathServiceImpl);    // We create a service

    Server<ProtocolBuffersChannel> server(io_service, 8080);    // We create a simple server that uses our channel
    server.add_service(service);    // We attach the service to the server
    server.start_accept();

    io_service.run();
}
