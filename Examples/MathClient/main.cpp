
#include <boost/shared_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <iostream>

#include "rpc/protocolbufferschannel.h"
#include "rpc/server.h"
#include "math.pb.h"

using namespace std;
using namespace protorpc;

class Client : public math::Math_Stub {
public:
    Client(boost::shared_ptr<ProtocolBuffersChannel> channel) :
        math::Math_Stub(channel.get()),
        _channel(channel),
        _timer(channel->io_service()){
    }

    void start(string host, string port) {
         _channel->resolve_connect_and_start(host, port,
                                            boost::bind(&Client::handle_connect, this,
                                                         asio::placeholders::error));
    }

    virtual void string_received(const string &msg) {
        cerr << msg << endl;
    }

private:

    void handle_connect(const boost::system::error_code &error) {
        if(error) {
            cerr << "Error " << endl;
            return;
        }

        _timer.expires_from_now(boost::posix_time::milliseconds(5));
        _timer.async_wait(boost::bind(&Client::call_operations, this,
                                     asio::placeholders::error));
    }

    void call_operations(const boost::system::error_code &error) {

        // Adding
        pb::RpcController *cAdd = new ProtocolBuffersController;
        math::MathBinaryOperationRequest request;
        request.set_first(3);
        request.set_second(5);
        math::MathResponse *responseAdd = new math::MathResponse;
        cerr << "I want to add 3 and 5. Calling RPC" << endl;
        this->Add(cAdd, &request, responseAdd, pb::NewCallback(this, &Client::handle_add, cAdd, responseAdd));

        // Substracting
        pb::RpcController *cSubstract = new ProtocolBuffersController;
        request.set_first(3);
        request.set_second(5);
        math::MathResponse *responseSubstract = new math::MathResponse;
        cerr << "I want to substract 3 and 5. Calling RPC" << endl;
        this->Substract(cSubstract, &request, responseSubstract, pb::NewCallback(this, &Client::handle_substract, cSubstract, responseSubstract));

        // Multiplying
        pb::RpcController *cMultiply = new ProtocolBuffersController;
        request.set_first(3);
        request.set_second(5);
        math::MathResponse *responseMultiply = new math::MathResponse;
        cerr << "I want to multiply 3 and 5. Calling RPC" << endl;
        this->Multiply(cMultiply, &request, responseMultiply, pb::NewCallback(this, &Client::handle_multiply, cMultiply, responseMultiply));

        // Dividing
        pb::RpcController *cDivide = new ProtocolBuffersController;
        request.set_first(3);
        request.set_second(5);
        math::MathResponse *responseDivide = new math::MathResponse;
        cerr << "I want to divide 3 and 5. Calling RPC" << endl;
        this->Divide(cDivide, &request, responseDivide, pb::NewCallback(this, &Client::handle_divide, cDivide, responseDivide));

        // Dividing by 0
        pb::RpcController *cDivideBy0 = new ProtocolBuffersController;
        request.set_first(3);
        request.set_second(0);
        math::MathResponse *responseDivideBy0 = new math::MathResponse;
        cerr << "I want to add 3 and 0. Calling RPC" << endl;
        this->Divide(cDivideBy0, &request, responseDivideBy0, pb::NewCallback(this, &Client::handle_divide, cDivideBy0, responseDivideBy0));

        _timer.expires_from_now(boost::posix_time::milliseconds(5));
        _timer.async_wait(boost::bind(&Client::call_operations, this,
                                     asio::placeholders::error));
    }

    void handle_add(pb::RpcController *controller, math::MathResponse *response) {
        cerr << "RPC returned for adding" << endl;
        if(controller->Failed()) {
            cerr << "Error when adding: " << controller->ErrorText() << endl;
        } else {
            cerr << "Adding result = " << response->result() << endl;
        }
        delete response;
        delete controller;
    }

    void handle_substract(pb::RpcController *controller, math::MathResponse *response) {
        cerr << "RPC returned for substraction" << endl;
        if(controller->Failed()) {
            cerr << "Error when substracting: " << controller->ErrorText() << endl;
        } else {
            cerr << "Substracting result = " << response->result() << endl;
        }
        delete response;
        delete controller;
    }

    void handle_multiply(pb::RpcController *controller, math::MathResponse *response) {
        cerr << "RPC returned for multiplying" << endl;
        if(controller->Failed()) {
            cerr << "Error when multiplying: " << controller->ErrorText() << endl;
        } else {
            cerr << "Multiplying result = " << response->result() << endl;
        }
        delete response;
        delete controller;
    }

    void handle_divide(pb::RpcController *controller, math::MathResponse *response) {
        cerr << "RPC returned for dividing" << endl;
        if(controller->Failed()) {
            cerr << "Error when dividing: " << controller->ErrorText() << endl;
        } else {
            cerr << "Dividing result = " << response->result() << endl;
        }
        delete response;
        delete controller;
    }

    boost::shared_ptr<ProtocolBuffersChannel> _channel;
    boost::asio::deadline_timer _timer;
};


int main(int argc, char *argv[])
{
    cerr << "connecting to 8080" << endl;
    asio::io_service io_service;

    boost::shared_ptr<ProtocolBuffersChannel> channel(new ProtocolBuffersChannel(io_service));
    Client client(channel);
    client.start("localhost", "8080");

    io_service.run();
}

