#-------------------------------------------------
#
# Project created by QtCreator 2011-11-11T17:17:05
#
#-------------------------------------------------

! include( ../../common.pri ) {
    error( Couldn't find the common.pri file! )
}

QT       += core

QT       -= gui

TARGET = EchoClientInt32StringProtocol
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

