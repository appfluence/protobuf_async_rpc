
#include <boost/bind.hpp>

#include "rpc/int32stringprotocol.h"

using namespace protorpc;
namespace asio = boost::asio;

class Client : Int32StringProtocol {
public:
    Client(asio::io_service &io_service) :
        Int32StringProtocol(io_service) {
    }

    void start(string host, string port) {
         resolve_connect_and_start(host, port,
                                            boost::bind(&Client::handle_connect, this,
                                                         asio::placeholders::error));
    }

private:

    void handle_connect(const boost::system::error_code &error) {
        if(error) {
            cerr << "Error " << endl;
        }

        for(int i = 0; i < 10000; i++)
            send_string("hello");
    }

    virtual void string_received(const string &msg) {
        cerr << "Client received: " << msg << endl;
    }
};


int main(int argc, char *argv[])
{
    cerr << "connecting to 8080" << endl;
    asio::io_service io_service;

    Client client(io_service);
    client.start("localhost", "8080");

    io_service.run();
}

