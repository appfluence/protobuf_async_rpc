#ifndef PROTOCOLBUFFERSCHANNEL_H
#define PROTOCOLBUFFERSCHANNEL_H

#include "int32stringprotocol.h"
#include "protobufrpc.pb.h"

#include "common.h"

#include <assert.h>

#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string.hpp>

#include <google/protobuf/service.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/message.h>

using namespace std;
namespace pb = ::google::protobuf;

namespace protorpc {

#define DEBUGPROTOCOLBUFFERSCHANNEL 1

#if DEBUGPROTOCOLBUFFERSCHANNEL
    #include <iostream>
#endif

class ProtocolBuffersChannel : public Int32StringProtocol, public pb::RpcChannel
{
public:
    typedef boost::shared_ptr<ProtocolBuffersChannel> Pointer;

    ProtocolBuffersChannel(asio::io_service& io_service) :
        Int32StringProtocol(io_service), pb::RpcChannel() {

        _last_request_id = 0;
    }

    void add_service(boost::shared_ptr<pb::Service> service) {
        _services[service->GetDescriptor()->name()] = service;
    }

    virtual void error_received(const boost::system::error_code &error) {
        DEBUGPROTOCOLBUFFERSCHANNEL && (cerr << "Error received: " << error.message() << endl);
    }

protected:

    virtual void CallMethod(const pb::MethodDescriptor* method,
                          pb::RpcController* controller,
                          const pb::Message* request,
                          pb::Message* response,
                          pb::Closure* done) {
        boost::shared_ptr<Rpc> rpc = _call_method(method, controller, request, response, done);
        send_string(rpc->SerializeAsString());
    }

    virtual void string_received(const string &data)
    {
        Rpc rpc;
        if(!rpc.ParseFromString(data)) {
            error_received(boost::system::error_code(protorpc_category::unserialize_rpc,
                                                     _protorpc_category));
            return;
        }

        for(int i = 0; i < rpc.request_size(); i++) {
            const Request &serialized_request = rpc.request(i);
            std::vector<std::string> splits;
            boost::split(splits, serialized_request.method(), boost::is_any_of("."));

            if(_services.find(splits[0]) == _services.end()) {  // SERVICE NOT FOUND
                send_error(serialized_request.id(),
                           boost::system::error_code(protorpc_category::service_not_found,
                                                     _protorpc_category));
                return;
            }

            boost::shared_ptr<pb::Service> service = _services[splits[0]];
            const pb::MethodDescriptor *method = service->GetDescriptor()->FindMethodByName(splits[1]);

            if(!method) {       // METHOD NOT FOUND
                send_error(serialized_request.id(),
                           boost::system::error_code(protorpc_category::method_not_found,
                                                     _protorpc_category));
                return;
            }

            pb::Message *request = service->GetRequestPrototype(method).New();

            if(!request->ParseFromString(serialized_request.serialized_request())) {    // REQUEST UNSERIALIZE
                send_error(serialized_request.id(),
                           boost::system::error_code(protorpc_category::cannot_deserialize_request,
                                                     _protorpc_category));
                return;
            }

            pb::Message *response = service->GetResponsePrototype(method).New();

            ProtocolBuffersController *controller = new ProtocolBuffersController;

            boost::shared_ptr<RpcCallbackParameters> parameters = boost::shared_ptr<RpcCallbackParameters>(new RpcCallbackParameters);;
            parameters->request = request;
            parameters->response = response;
            parameters->controller = controller;
            parameters->request_id = serialized_request.id();

            pb::Closure *done = pb::NewCallback(this, &ProtocolBuffersChannel::handle_response, parameters);

            service->CallMethod(method, controller, request, response, done);
        }

        for(int i = 0; i < rpc.response_size(); i++) {

            const Response &serialized_response = rpc.response(i);
            unsigned int id = serialized_response.id();

            pb::Message *response = _pending_responses[id];
            _pending_responses.erase(id);
            pb::Closure *done = _pending_closures[id];
            _pending_closures.erase(id);
            pb::RpcController *controller = _pending_controllers[id];
            _pending_controllers.erase(id);


            if(!serialized_response.has_error() && serialized_response.error().code() == 0) {
                response->ParseFromString(serialized_response.serialized_response());
            } else {
                controller->SetFailed(serialized_response.error().text());
            }

            done->Run();
        }
    }


    virtual boost::shared_ptr<Rpc> _call_method(const pb::MethodDescriptor* method,
                                                pb::RpcController *controller,
                                                const pb::Message* request,
                                                pb::Message* response,
                                                pb::Closure* done) {
        _last_request_id++;

        boost::shared_ptr<Rpc> rpc(new Rpc);
        Request *rpc_request = rpc->add_request();
        rpc_request->set_method(method->service()->name() + "." + method->name());
        rpc_request->set_serialized_request(request->SerializeAsString());
        rpc_request->set_id(_last_request_id);

        _pending_responses[_last_request_id] = response;
        _pending_closures[_last_request_id] = done;
        controller->Reset();
        _pending_controllers[_last_request_id] = controller;

        return rpc;
    }

    void handle_response(boost::shared_ptr<RpcCallbackParameters> parameters) {
        Rpc rpc;
        Response *serialized_response = rpc.add_response();
        serialized_response->set_id(parameters->request_id);

        pb::RpcController *controller = parameters->controller;

        if(controller->Failed()) {
            Error * error = serialized_response->mutable_error();
            error->set_code(1);
            error->set_text(controller->ErrorText());
        } else {
            serialized_response->set_serialized_response(parameters->response->SerializeAsString());
        }

        send_string(rpc.SerializeAsString());

        delete parameters->response;
        delete parameters->request;
        delete parameters->controller;
    }

private:
    map<string, boost::shared_ptr<pb::Service> > _services;
    map<unsigned int, pb::Message *> _pending_responses;
    map<unsigned int, pb::Closure *> _pending_closures;
    map<unsigned int, pb::RpcController *> _pending_controllers;
    protorpc_category _protorpc_category;

    unsigned int _last_request_id;

    void send_error(unsigned int request_id, const boost::system::error_code &ec) {
        DEBUGPROTOCOLBUFFERSCHANNEL && (cerr << ec << endl);
        Rpc rpc;
        Response *response = rpc.add_response();
        response->set_id(request_id);
        Error *error =response->mutable_error();
        error->set_code(ec.value());
        error->set_text(ec.message());
        send_string(rpc.SerializeAsString());
    }
};

}   // NAMESPACE APPFLUENCE

#endif // PROTOCOLBUFFERSCHANNEL_H
