/***
  Implementation of a simple protocol asynchronously with boost::asio
  The protocol prepends an int with the string size before sending and it is able to read
  as well

  Link with: -lboost_system
  */
#ifndef INT32STRINGPROTOCOL_H
#define INT32STRINGPROTOCOL_H

#include "common.h"

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/cstdint.hpp>
#include <boost/function.hpp>
#include <boost/tuple/tuple.hpp>

#include <iostream>
#include <string>
#include <deque>

#define DEBUGINT32STRINGPROTOCOL (1 && (cerr << "Int32StringProtocol: "))

using namespace std;
namespace asio = boost::asio;
using asio::ip::tcp;
using boost::uint8_t;

namespace protorpc {

typedef std::vector<boost::uint8_t> data_buffer;

// The header size for packed messages
//
const unsigned HEADER_SIZE = 4;

class Int32StringProtocol : public StringProtocol
{
public:
    typedef boost::shared_ptr<Int32StringProtocol> Pointer;

    Int32StringProtocol(asio::io_service& io_service)
        : m_socket(io_service),
          m_io_service(io_service)
    {
        connection_lost_called = connected = closing = reading = false;
        write_operation = 0;
    }

    virtual void resolve_connect_and_start(string host, string port, boost::function<void (boost::system::error_code)> callback = 0) {

        reading = true;
        m_resolver = boost::shared_ptr<tcp::resolver>(new tcp::resolver(m_io_service));
        m_query = boost::shared_ptr<tcp::resolver::query>(new tcp::resolver::query(host, port));
        m_resolver->async_resolve(*m_query,
                                boost::bind(&Int32StringProtocol::handle_resolve, this,
                                            boost::asio::placeholders::error,
                                            boost::asio::placeholders::iterator,
                                            callback));
    }

    virtual bool start() {

        if(!m_socket.is_open()) {
            return false;
        }

        connected = true;
        start_read_header();
        start_writing();

        return true;
    }

    // Called when enough data was read into m_readbuf for a complete request
    // message.
    // Parse the request, execute it and send back a response.
    //
    virtual void string_received(const string &msg)
    {
        DEBUGINT32STRINGPROTOCOL && (cerr << msg << endl);
    }

    virtual unsigned int send_string(const string &msg,
                                     boost::function<void (unsigned int operation,
                                                           boost::system::error_code)> callback = 0) {
        bool write_in_progress = !m_write_queue.empty();
        m_write_queue.push_back(
                                boost::tuples::tuple<string,
                                                     unsigned int,
                                                     boost::function<void (unsigned int operation, boost::system::error_code)> >
                                                            (msg, write_operation, callback)
                    );

        if(!write_in_progress && connected) {
            start_writing();
        }

        return write_operation++;
    }

    virtual void error_received(const boost::system::error_code &error) {
        DEBUGINT32STRINGPROTOCOL && (cerr << error << endl);
    }


    virtual void connection_lost() {
    }

    virtual void close() {
        DEBUGINT32STRINGPROTOCOL && (cerr << "Closing now" << endl);
        closing = true;
        connected = false;
        call_connection_lost_only_once();
        m_socket.close();
    }

    virtual void nice_close() {

        if(!m_write_queue.empty() || reading) {
            DEBUGINT32STRINGPROTOCOL && (cerr << "Write or read in progress" << endl);
            closing = true;
        } else {
            close();
        }
    }

    virtual tcp::socket &socket() {
        return m_socket;
    }

    virtual asio::io_service &io_service() {
        return m_io_service;
    }

private:
    tcp::socket m_socket;
    data_buffer m_readbuf;
    deque<boost::tuples::tuple<string, unsigned int, boost::function<void (unsigned int, boost::system::error_code)> > > m_write_queue;

    // States
    bool connected, closing, reading, connection_lost_called;
    asio::io_service &m_io_service;
    boost::shared_ptr<tcp::resolver> m_resolver;
    boost::shared_ptr<tcp::resolver::query> m_query;
    unsigned int write_operation;


protected:
    void handle_resolve(const boost::system::error_code &error, tcp::resolver::iterator endpoint_iterator,
                        boost::function<void (boost::system::error_code)> callback = 0)
    {
        if(!error) {
            boost::asio::async_connect(m_socket, endpoint_iterator,
                                   boost::bind(&Int32StringProtocol::handle_connect, this,
                                               boost::asio::placeholders::error,
                                               callback));
        } else {
            error_received(error);

            call_connection_lost_only_once();

            if(callback)
                callback(error);
        }
    }

    void handle_connect(const boost::system::error_code& error, boost::function<void (boost::system::error_code)> callback = 0)
    {
        reading = false;
        if (!error)
        {
            connected = true;
            start_read_header();
            start_writing();
        } else {
            error_received(error);
            call_connection_lost_only_once();
        }

        if(callback)
            callback(error);
    }

    void start_writing() {
        if (!m_write_queue.empty() && connected)
        {
            DEBUGINT32STRINGPROTOCOL && (cerr << "Start writing" << endl);
            string &msg = m_write_queue.front().get<0>();
            data_buffer buffer;
            prefixed_message(buffer, msg);
            boost::asio::async_write(m_socket,
                                     boost::asio::buffer(buffer, buffer.size()),
                                     boost::bind(&Int32StringProtocol::handle_write, this,
                                                 m_write_queue.front().get<1>(),
                                                 m_write_queue.front().get<2>(),
                                                 boost::asio::placeholders::error));
       }
    }



    void handle_read_header(const boost::system::error_code& error)
    {
        reading = true;

        DEBUGINT32STRINGPROTOCOL && (cerr << "handle read " << error.message() << '\n');
        if (!error) {
            DEBUGINT32STRINGPROTOCOL && (cerr << "Got header!\n");
            DEBUGINT32STRINGPROTOCOL && (cerr << show_hex(m_readbuf) << endl);
            unsigned msg_len = decode_header(m_readbuf);
            DEBUGINT32STRINGPROTOCOL && (cerr << msg_len << " bytes\n");
            start_read_body(msg_len);
        } else {
            error_received(error);
            call_connection_lost_only_once();
        }
    }

    void handle_read_body(const boost::system::error_code& error)
    {
        DEBUGINT32STRINGPROTOCOL && (cerr << "handle body " << error << '\n');
        reading = false;

        if (!error) {
            DEBUGINT32STRINGPROTOCOL && (cerr << "Got body!\n");
            DEBUGINT32STRINGPROTOCOL && (cerr << show_hex(m_readbuf) << endl);
            string_received(string(m_readbuf.begin() + HEADER_SIZE, m_readbuf.end()));
            start_read_header();
        } else {
            error_received(error);
            call_connection_lost_only_once();
        }

        if(closing && m_write_queue.empty()) {
            DEBUGINT32STRINGPROTOCOL && (cerr << "Closing connection in handle_read_body" << endl);
            close();
        }
    }

    void start_read_header()
    {

        m_readbuf.resize(HEADER_SIZE);
        asio::async_read(m_socket, asio::buffer(m_readbuf),
                boost::bind(&Int32StringProtocol::handle_read_header, this,
                            asio::placeholders::error));
    }

    void start_read_body(unsigned msg_len)
    {
        // m_readbuf already contains the header in its first HEADER_SIZE
        // bytes. Expand it to fit in the body as well, and start async
        // read into the body.
        //
        m_readbuf.resize(HEADER_SIZE + msg_len);
        asio::mutable_buffers_1 buf = asio::buffer(&m_readbuf[HEADER_SIZE], msg_len);
        asio::async_read(m_socket, buf,
                boost::bind(&Int32StringProtocol::handle_read_body, this,
                    asio::placeholders::error));
    }


    void handle_write(unsigned int operation_num,
                      boost::function<void (unsigned int, boost::system::error_code)> callback,
                      const boost::system::error_code& error)
    {
        DEBUGINT32STRINGPROTOCOL && (cerr << "handle_write" << endl);
        if (!error && connected)
        {
            m_write_queue.pop_front();

            start_writing();
       }
        else
        {
            error_received(error);
        }

        if(callback)
            callback(operation_num, error);

        if(closing && !reading && m_write_queue.empty()) {
            DEBUGINT32STRINGPROTOCOL && (cerr << "Closing connection in handle_write" << endl);
            close();
        }
    }

    // Given a buffer with the first HEADER_SIZE bytes representing the header,
    // decode the header and return the message length. Return 0 in case of
    // an error.
    //
    unsigned decode_header(const data_buffer& buf) const
    {
        if (buf.size() < HEADER_SIZE)
            return 0;
        unsigned msg_size = 0;
        for (unsigned i = 0; i < HEADER_SIZE; ++i)
            msg_size = msg_size * 256 + (static_cast<unsigned>(buf[i]) & 0xFF);
        return msg_size;
    }

    void prefixed_message(data_buffer &buffer, string msg) {
        buffer.clear();
        encode_header(buffer, msg.size());
        buffer.insert(buffer.end(), msg.begin(), msg.end());
    }

    // Encodes the side into a header at the beginning of buf
    //
    void encode_header(data_buffer& buf, unsigned size) const
    {
        buf.resize(HEADER_SIZE);
        buf[0] = static_cast<boost::uint8_t>((size >> 24) & 0xFF);
        buf[1] = static_cast<boost::uint8_t>((size >> 16) & 0xFF);
        buf[2] = static_cast<boost::uint8_t>((size >> 8) & 0xFF);
        buf[3] = static_cast<boost::uint8_t>(size & 0xFF);
    }

    void call_connection_lost_only_once() {
        if(!connection_lost_called) {
            connection_lost_called = true;
            connection_lost();
        }
    }
};

}   // NAMESPACE APPFLUENCE

#endif // INT32STRINGPROTOCOL_H
