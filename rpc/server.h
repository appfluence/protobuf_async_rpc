#ifndef PROTOCOLBUFFERSSERVER_H
#define PROTOCOLBUFFERSSERVER_H

#include "protocolbufferschannel.h"

#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include <iostream>
#include <map>
#include <vector>

using namespace boost::asio::ip;

namespace asio = boost::asio;

namespace protorpc {

template <typename Protocol>
class Server {

public:
    Server(asio::io_service &io_service, unsigned short port = 8080)
        : _acceptor(io_service, tcp::endpoint(tcp::v4(), port)) {
    }

    void add_service(boost::shared_ptr<pb::Service> service) {
        _services[service->GetDescriptor()->name()] = service;
    }

    void start_accept() {
        boost::shared_ptr<Protocol> channel =
                boost::shared_ptr<Protocol>(new Protocol(_acceptor.get_io_service()));

        for(map<string, boost::shared_ptr<pb::Service> >::iterator it = _services.begin(); it != _services.end(); ++it) {
            channel->add_service((*it).second);
        }
        _acceptor.async_accept(channel->socket(), boost::bind(&Server::handle_accept, this,
                                                              channel, boost::asio::placeholders::error));
    }

private:
    asio::ip::tcp::acceptor _acceptor;
    map<string, boost::shared_ptr<pb::Service> > _services;


    void handle_accept(boost::shared_ptr<Protocol> channel, const boost::system::error_code &error) {
        if(!error) {
            channel->start();
            open_channels.push_back(channel);
        } else {
            std::cerr << "Error when accepting connection" << std::endl;
        }

        start_accept();
    }

    std::vector<boost::shared_ptr<Protocol> > open_channels;
};

typedef Server<ProtocolBuffersChannel> ProtocolBuffersServer;

}   // NAMESPACE APPFLUENCE


#endif // PROTOCOLBUFFERSSERVER_H
