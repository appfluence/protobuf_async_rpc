#ifndef COMMON_H
#define COMMON_H

#include <boost/system/error_code.hpp>
#include <google/protobuf/service.h>
#include <google/protobuf/message.h>

#include <boost/asio.hpp>
#include <boost/function.hpp>

#include "protobufrpc.pb.h"

using namespace std;

namespace protorpc {

// A generic function to show contents of a container holding byte data
// as a string with hex representation for each byte.
//
template <class CharContainer>
std::string show_hex(const CharContainer& c)
{
    std::string hex;
    char buf[16];
    typename CharContainer::const_iterator i;
    for (i = c.begin(); i != c.end(); ++i) {
        std::sprintf(buf, "%02X ", static_cast<unsigned>(*i) & 0xFF);
        hex += buf;
    }
    return hex;
}

class StringProtocol {
public:
    virtual void resolve_connect_and_start(string host, string port,
                                           boost::function<void (boost::system::error_code)> callback = 0) = 0;
    virtual bool start() = 0;

    // Callbacks
    virtual void string_received(const string &msg) = 0;
    virtual void error_received(const boost::system::error_code &error) = 0;
    virtual void connection_lost() = 0;

    // Closing
    virtual void close() = 0;
    virtual void nice_close() = 0;

    // Send stuff
    virtual unsigned int send_string(const string &msg,
                                     boost::function<void (unsigned int operation,
                                                           boost::system::error_code)> callback = 0) = 0;

    // Accessing socket
    virtual boost::asio::ip::tcp::socket &socket() = 0;
};

static const string error_msgs[] = {
    "Success",
    "Error when unserializing Rpc message",
    "Service not found",
    "Method not found",
    "Cannot deserialized request"
};

class protorpc_category : public boost::system::error_category {
public:
    enum errors {
        success = 0,
        unserialize_rpc,
        service_not_found,
        method_not_found,
        cannot_deserialize_request
    };


    const char *name() const { return "protocol buffers rpc"; }
    std:: string message(int ev) const { return error_msgs[ev]; }
};

class ProtocolBuffersController : public google::protobuf::RpcController {
public:
    ProtocolBuffersController() {
        failed = false;
    }

    virtual void Reset() {
        failed = false;
        error_text = "";
    }

  // After a call has finished, returns true if the call failed.  The possible
  // reasons for failure depend on the RPC implementation.  Failed() must not
  // be called before a call has finished.  If Failed() returns true, the
  // contents of the response message are undefined.
    virtual bool Failed() const {
        return failed;
    }

  // If Failed() is true, returns a human-readable description of the error.
    virtual string ErrorText() const {
        return error_text;
    }

  // Advises the RPC system that the caller desires that the RPC call be
  // canceled.  The RPC system may cancel it immediately, may wait awhile and
  // then cancel it, or may not even cancel the call at all.  If the call is
  // canceled, the "done" callback will still be called and the RpcController
  // will indicate that the call failed at that time.
    virtual void StartCancel() {
    }

  // Server-side methods ---------------------------------------------
  // These calls may be made from the server side only.  Their results
  // are undefined on the client side (may crash).

  // Causes Failed() to return true on the client side.  "reason" will be
  // incorporated into the message returned by ErrorText().  If you find
  // you need to return machine-readable information about failures, you
  // should incorporate it into your response protocol buffer and should
  // NOT call SetFailed().
    virtual void SetFailed(const string& reason) {
        failed = true;
        error_text = reason;
    }

  // If true, indicates that the client canceled the RPC, so the server may
  // as well give up on replying to it.  The server should still call the
  // final "done" callback.
    virtual bool IsCanceled() const {
        return false;
    }

  // Asks that the given callback be called when the RPC is canceled.  The
  // callback will always be called exactly once.  If the RPC completes without
  // being canceled, the callback will be called after completion.  If the RPC
  // has already been canceled when NotifyOnCancel() is called, the callback
  // will be called immediately.
  //
  // NotifyOnCancel() must be called no more than once per request.
    virtual void NotifyOnCancel(google::protobuf::Closure* callback) {
    }
private:
    bool failed;
    string error_text;
};

struct RpcCallbackParameters {
    google::protobuf::Message *request;
    google::protobuf::Message *response;
    google::protobuf::RpcController *controller;
    unsigned int request_id;
};

}   // NAMESPACE APPFLUENCE

#endif // COMMON_H
